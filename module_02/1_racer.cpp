#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int lap = 4;
    int engine = 254;
    int tires = 93;
    int handlebar = 49;
    int wind = 21;
    int rain = 17;
    int speed = engine + tires + handlebar - wind - rain;


    std::cout << "===================\n";
    std::cout << "Супер гонки. Круг " << lap << "\n";
    std::cout << "===================\n";
    std::cout << "Шумахер (" << speed << ")\n";
    std::cout << "===================\n";
    std::cout << "Водитель: Шумахер\n";
    std::cout << "Скорость: " << speed << "\n";
    std::cout << "-------------------\n";
    std::cout << "Оснащение\n";
    std::cout << "Двигатель: +" << engine << "\n";
    std::cout << "Колёса: +" << tires << "\n";
    std::cout << "Руль: +" << handlebar << "\n";
    std::cout << "-------------------\n";
    std::cout << "Действия плохой погоды\n";
    std::cout << "Ветер: -" << wind << "\n";
    std::cout << "Дождь: -" << rain << "\n";    
    
    
    
    system("pause");
    return 0;
}



