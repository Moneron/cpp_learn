#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int receiptSum = 4000000;
    int entranceCount = 10;
    int flatsPerEntrance = 40;
    int flatFee = receiptSum / (entranceCount * flatsPerEntrance);

    std::cout << "Приветствуем вас в калькуляторе квартплаты!\n\n";
    std::cout << "Введите сумму, указанную в квитанции: " << receiptSum << "\n";
    std::cout << "Сколько подъездов в вашем доме? " << entranceCount << "\n";
    std::cout << "Сколько квартир в каждом подъезде? " << flatsPerEntrance << "\n";
    std::cout << "-----Считаем-----\n";
    std::cout << "Каждая квартира должна платить по " << flatFee << " руб.\n";
    
    
    
    system("pause");
    return 0;
}


