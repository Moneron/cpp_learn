#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int productCost = 12000;
    int deliveryCost = 1300;
    int discount = 200;
    int totalCost = productCost + deliveryCost - discount;
    std::cout << "Стоимость продукта: " << productCost << " руб. \nСтоимость доставки: " << deliveryCost << " руб.\n";
    std::cout << "Скидка составила: " << discount << " руб.\n" << "ИТОГО: " << totalCost << "руб\n";
    
    system("pause");
    return 0;
}

