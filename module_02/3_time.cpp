#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int shiftDuration = 480;
    int clientChoosing = 2;
    int cashierAssembling = 4;
    int clientsPerShift = shiftDuration / (clientChoosing + cashierAssembling);


    std::cout << "Эта программа рассчитает, сколько клиентов успеет обслужить кассир за смену.\n";
    std::cout << "Введите длительность смены в минутах: " << shiftDuration << "\n";
    std::cout << "Сколько минут клиент делает заказ? " << clientChoosing << "\n";
    std::cout << "Сколько минут кассир собирает заказ? " << cashierAssembling << "\n";
    std::cout << "-----Считаем-----\n";
    std::cout << "За смену длиной " << shiftDuration << " минут кассир успеет обслужить " << clientsPerShift << " клиентов.\n";    
    

    system("pause");
    return 0;
}

