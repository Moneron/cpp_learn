#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int beginHeight = 100;
    int dailyGrowth = 50;
    int nightFade = 20;
    int days;

    std::cout << "Сколько дней растёт бамбук? ";
    std::cin >> days;
    int currHeight = beginHeight + (dailyGrowth - nightFade) * (days - 1) + dailyGrowth / 2;
    std::cout << "К середине " << days << " дня высота бамбука составит " << currHeight << " см.\n";
    
    
    
    system("pause");
    return 0;
}
