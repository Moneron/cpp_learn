#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int a = 42;
    int b = 153;
    std::cout << "a: " << a << "\n"; /* На экран будет выведено 42 */
    std::cout << "b: " << b << "\n"; /* На экран будет выведено 153 */

    /* Меняем значения */
    b += a;
    a = b - a;
    b -= a;

    std::cout << "a: " << a << "\n"; /* На экран будет выведено 153 */
    std::cout << "b: " << b << "\n"; /* На экран будет выведено 42 */
    
    
    
    system("pause");
    return 0;
}
