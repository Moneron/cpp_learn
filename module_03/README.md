## 3.5 ������������ ������

### ������ 1. ��������� ���������
**��� ����� �������**

�������� ���������, ������� ���������� ������ ���������. ��� ����� ��������� ��������� � �������� � ������������, ������� ������� ����� � ����� �� ���������. ��������� ���������� ������ ��� �����. �������� ��� ������ �������� ���:
```
��������� �� ��������� ������ �������������. � ������ ����������: 0
������� ���������� ����� �� ���������? 0
������� ���������� ����� �� ���������? 3
������������ � ��������� ������ �������������. � ������ ����������: 3
-----------����---------
��������� �� ��������� ��������� ����������. � ������ ����������: 3
�
�
```

������� ������� �� ������ ���������. �������� ��������� ���, ����� � ����� ��� ��������, ������� ����� �� ���������� ��� �������, ��� ����� ����� 20 ���., � ������� ���������:

* �������� � �� �������� ��������;
* ����� ����� � �� �������;
* ����� ����� � �� ������;
* ����� ����� � �� ������ ������.

������ ������ (����� �������� �� ��������� ���������):
```
����� ����������: 100 ���.
�������� ��������: 25 ���.
������� �� �������: 20 ���.
������: 20 ���.
������� �� ������ ������: 20 ���.
����� �����: 15 ���.
```
������������ �� ����������:<br>
* � ����� ��������� ������ ���� ��� ���� ���������� �������� � �������� ����������, ��� � ������� ������� � ��������.
* ����� ������� �������� ��������� � ����������.
* ������������ �������� ��������� ���, ����� � �� ��� ������ ��������� ����� ���� �������� ��� ��������� ���������.

��� �����������:

* ������������ �������� �������� ���������� ����������.
* ������������ �������� ������� � ��������.
* ���������� ������ ����������.

### ������ 2. ����� �������
**��� ����� �������**

���� ��� ��������� � ����� ����������� ���� `int` � ������� ���� ���������� �� �����. �������� ���������, ������� ������ �������� ���������� �������, �� ���� ����� �������� ����, ����� � ���������� `a` ����� �������� `b`, � � `b` � �������� `a`.

������ ��������� 
```cpp
int main() {
int a = 42;
int b = 153;
std::cout << "a: " << a << "\n"; /* �� ����� ����� �������� 42 */
std::cout << "b: " << b << "\n"; /* �� ����� ����� �������� 153 */

/* ������ �������� */

std::cout << "a: " << a << "\n"; /* �� ����� ����� �������� 153 */
std::cout << "b: " << b << "\n"; /* �� ����� ����� �������� 42 */
}
```

**��� �����������**

�� ��������� ������ ��������� ������ � ���������� a ������ ������ ��������, ������� ���������� ������ � ���������� b, � ��������.


### ������ 3. �������� ���������
**��� ����� �������**

�� ���������� ��������� �������� ��������. ��� ���� ���� � ���� ������ �����. ������ �����-����������������, ����� ������ ��� ����� ���, ���� ����� ������ � �� 50 ����������� ���������! �������� ������� 20 ����������� ������� ������ ����. ���������� ������� ��� ������� ����� ����� ������ ���� ����. 

�������� ���������, ������� �������, ����� ������ ����� ������ � �������� �������� ���. ���������� � ���������� ��� �� �� ��������� ��������, � ����� ��������.

**������ � ������������**

* ����������� ����������� � ���� ���������.
* ��� ������� ��������� � �����������.

��� �����������

* ������������ �������� ������ ������� � �������� ���.
* �����������, ���� � ������� ������������ ���������������� ���� ��� ������� �������� ����������.

���������<br>
������ ������� � �������� �������� ��� ����� ��������� � ������� �������:

`beginHeight + (dailyGrowth - nightFade) * 2 + dailyGrowth / 2;`

� ���:

`dailyGrowth` � ���������� ����;<br>
`nightFade` � ������ ���������� ���������� ������� �� ���� ����;<br>
`beginHeight` � ��������� ������ �������.

### ������ 4 (��������������). �������� ������ ���������*
**��� ����� �������**<br>
��������� � ������ ��� ����� ������� �������� ���������� � ���������� ������ � ��� ������������� ������� ���������� (���, �������, �� ������ ���������!). �����������, � ����� ������� ����� ������ ����� �� �������� � ����� � ���� ������������.

������ � ������������
* ����������� ������ ��� �������� ����������.
* ����������� �������������� �������� ��� ������.

**��� �����������**<br>
�� ��������� ������ ��������� ������ � ���������� a ������ ������ ��������, ������� ���������� ������ � ���������� b, � ��������.
� ��������� ������������ ������ ��� ����������.
