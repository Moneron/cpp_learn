#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    std::string stop1 = "\"Улица программистов\"";
    std::string stop2 = "\"Улица Алгоритмов\"";
    std::string stop3 = "\"Улица рекурсии\"";
    std::string stop4 = "\"Улица пайтоник вей\"";

    int curPassengersCount = 0;
    int cost = 20;
    int passengersIn;
    int totalPassengersIn;
    int passengersOut;
    int income;
    int earnings;


    std::cout << "Прибываем на остановку " << stop1 << ". В салоне пассажиров: " << curPassengersCount << "\n";
    std::cout << "Сколько пассажиров вышло на остановке? ";
    std::cin >> passengersOut;
    curPassengersCount -= passengersOut;
    std::cout << "Сколько пассажиров зашло на остановке? ";
    std::cin >> passengersIn;
    curPassengersCount += passengersIn;
    totalPassengersIn += passengersIn;
    std::cout << "-----------Едем---------\n\n";

    std::cout << "Прибываем на остановку " << stop2 << ". В салоне пассажиров: " << curPassengersCount << "\n";
    std::cout << "Сколько пассажиров вышло на остановке? ";
    std::cin >> passengersOut;
    curPassengersCount -= passengersOut;
    std::cout << "Сколько пассажиров зашло на остановке? ";
    std::cin >> passengersIn;
    curPassengersCount += passengersIn;
    totalPassengersIn += passengersIn;
    std::cout << "-----------Едем---------\n\n";
    
    std::cout << "Прибываем на остановку " << stop3 << ". В салоне пассажиров: " << curPassengersCount << "\n";
    std::cout << "Сколько пассажиров вышло на остановке? ";
    std::cin >> passengersOut;
    curPassengersCount -= passengersOut;
    std::cout << "Сколько пассажиров зашло на остановке? ";
    std::cin >> passengersIn;
    curPassengersCount += passengersIn;
    totalPassengersIn += passengersIn;
    std::cout << "-----------Едем---------\n\n";
    
    std::cout << "Прибываем на остановку " << stop4 << ". В салоне пассажиров: " << curPassengersCount << "\n";
    std::cout << "Сколько пассажиров вышло на остановке? ";
    std::cin >> passengersOut;
    curPassengersCount -= passengersOut;
    std::cout << "Сколько пассажиров зашло на остановке? ";
    std::cin >> passengersIn;
    curPassengersCount += passengersIn;
    totalPassengersIn += passengersIn;

    income = totalPassengersIn * cost;
    int driverSalary = income / 4;
    int fuelCost = income / 5;
    int fee = income / 5;
    int amortization = income / 5;
    earnings = income - driverSalary - fuelCost - fee - amortization;
    std::cout << "\nВсего заработали " << income << " руб.\n";
    std::cout << "Зарплата водителя: " << driverSalary << " руб.\n";
    std::cout << "Расходы на топливо: " << fuelCost << " руб.\n";
    std::cout << "Налоги: " << fee << " руб.\n";
    std::cout << "Расходы на ремонт машины: " << amortization << " руб.\n";
    std::cout << "Итого доход: " << earnings << " руб.";
    
    
    
    system("pause");
    return 0;
}
