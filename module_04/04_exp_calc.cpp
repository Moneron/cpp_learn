#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int expirience, level;
    level = 1;
    std::cout << "Введите число очков опыта: ";
    std::cin >> expirience;
    if (expirience < 0){
        std::cout << "Опыт не может быть отрицательным\n";
        return 0;
    } else if (expirience >= 5000)
        level = 4;
    else if (expirience >= 2500)
        level = 3;
    else if (expirience >= 1000)
        level = 2;
    
    std::cout << "-----Считаем-----\n";
    std::cout << "Ваш уровень: " << level;
    
    
    
    system("pause");
    return 0;
}
