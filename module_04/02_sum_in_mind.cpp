#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int firstNum, secondNum, sum;

    std::cout << "Проверим, умеете ли вы считать в уме. Нужно будет сложить два числа\n";
    std::cout << "Введите первое число: ";
    std::cin >> firstNum;
    std::cout << "Введите второе число: ";
    std::cin >> secondNum;
    std::cout << "Введите их сумму: ";
    std::cin >> sum;
    std::cout << "-----Проверяем-----\n";

    if (sum == firstNum + secondNum)
        std::cout << "Верно\n";
    else
        std::cout << "Ошибка! Верный результат: " << firstNum + secondNum << "\n";

    
    
    
    system("pause");
    return 0;
}
