#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int userNum;
    std::cout << "Определим чётность числа\n";
    std::cout << "Введите число: ";
    std::cin >> userNum;

    if (userNum % 2 == 0)
        std::cout << "Число чётное!\n";
    else 
        std::cout << "Число нечётное!\n";
    
    
    
    system("pause");
    return 0;
}
