#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int income;
    int fee = 0;

    std::cout << "Рассчитаем налог. Введите сумму дохода: ";
    std::cin >> income;

    if (income > 50000){
        fee = ((income - 50000) * 30 + (50000 - 10000) * 20 + 10000 * 13) / 100;
    } else if (income > 10000){
        fee = ((income - 10000) * 20 + 10000 * 13) / 100;
    } else if (income > 10000 && income > 0){
        fee = income * 13 / 100;
    } else
        std::cout << "Нелегко же вам живётся...\n";

    if (fee)
        std::cout << "Сумма налога составит: " << fee << " руб.\n";
    else
        std::cout << "Скрываетесь, что ли? Товарищ майор бдит!";
    
    
    
    system("pause");
    return 0;
}
