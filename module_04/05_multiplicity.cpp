#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int firstNum, secondNum;
    std::cout << "Узнаем, делится ли первое число на второе без остатка\n";
    std::cout << "Введите первое число: ";
    std::cin >> firstNum;
    std::cout << "Введите второе число: ";
    std::cin >> secondNum;
    std::cout << "-----Проверяем-----\n";
    if (!(firstNum % secondNum))
        std::cout << "Да, " << firstNum << " делится на " << secondNum << " без остатка!\n";
    else
        std::cout << "Нет, " << firstNum << " не делится на " << secondNum << " без остатка!\n";
    
    
    
    system("pause");
    return 0;
}
