#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int firstSalary, secondSalary, thirdSalary, minSalary, maxSalary, avgSalary;

    std::cout << "Введите зарплату первого сотрудника: ";
    std::cin >> firstSalary;
    std::cout << "Введите зарплату второго сотрудника: ";
    std::cin >> secondSalary;
    std::cout << "Введите зарплату третьего сотрудника: ";
    std::cin >> thirdSalary;

    std::cout << "---------Считаем---------\n";

    minSalary = firstSalary;
    maxSalary = firstSalary;

    if (secondSalary < minSalary || thirdSalary < minSalary){       //Define minimum salary
        if (thirdSalary < secondSalary)
            minSalary = thirdSalary;
        else minSalary = secondSalary;
    } 

    if (secondSalary > maxSalary || thirdSalary > maxSalary){       //Define maximum salary
        if (thirdSalary > secondSalary)
            maxSalary = thirdSalary;
        else maxSalary = secondSalary;
    } 

    avgSalary = (firstSalary + secondSalary + thirdSalary) / 3;

    std::cout << "Самая высокая зарплата в отделе: " << maxSalary <<" рублей \n";
    std::cout << "Разница между самой высокой и самой низкой зарплатой в отделе: " 
                << maxSalary - minSalary <<" рублей \n";
    std::cout << "Средняя зарплата в отделе: " << avgSalary <<" рублей \n";
    
    
    
    system("pause");
    return 0;
}
