#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int firstNum, secondNum;
    std::cout << "Найдём наименьшее значение из двух чисел\n";
    std::cout << "Введите первое число: ";
    std::cin >> firstNum;
    std::cout << "Введите второе число: ";
    std::cin >> secondNum;
    std::cout << "-----Проверяем-----\n";
    if (firstNum < secondNum)
        std::cout << "Наименьшее число: " << firstNum;
    else if (secondNum < firstNum)
        std::cout << "Наименьшее число: " << secondNum;
    else 
        std::cout << "Числа равны!";
    
    
    
    system("pause");
    return 0;
}
