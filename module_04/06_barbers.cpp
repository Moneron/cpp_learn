#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int menCount, barberCount;
    std::cout << "*********** Барбершоп-калькулятор ***********\n";
    std::cout << "Введите количество мужчин в городе: ";
    std::cin >> menCount;

    std::cout << "Сколько барберов уже удалось нанять? ";
    std::cin >> barberCount;

    int menPerDay = menCount / 30;              //Постричь человек в день
    int menPerBarber = 8;
    int requiredBarbers = menPerDay / menPerBarber;
    if (menPerDay % menPerBarber)
        requiredBarbers += 1;

    std::cout << "Необходимое число барберов: " << requiredBarbers << "\n";

    if (requiredBarbers > barberCount)
        std::cout << "Нужно больше барберов!";
    else if (requiredBarbers < barberCount)
        std::cout << "Барберов даже больше чем нужно";
    else
        std::cout << "Барберов ровно столько, сколько нужно";
    
    
    
    system("pause");
    return 0;
}
