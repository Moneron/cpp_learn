#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int date;
    cout << "Введите число мая: ";
    cin >> date;

    if (date < 1 or date > 31) cout << "Введено некорректное число\n\n";
    else if ((date >= 1 and date <= 5) or (date >= 8 and date <= 10 ) or (date % 7 == 6 or date % 7 == 0))
        cout << "Выходной день\n\n";
    else cout << "Рабочий день\n\n";
    
    
    system("pause");
    return 0;
}
