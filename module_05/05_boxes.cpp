#include <iostream>
#include <algorithm>
#include <windows.h>

using namespace std;

bool __comp(int a, int b){
    return (a < b);
}

int __mid(int x, int y, int z){
    if ((x < y and y < z) or (z < y and y < x))
        return y;
    else if ((y < x and x < z) or (z < x and x < y))
        return x;
    else return z;
}

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    int a, b, c, m, n, k;
    cout << "Введите размеры первой коробки (три числа с пробелами): ";
    cin >> a >> b >> c;
    cout << "Введите размеры второй коробки (три числа с пробелами): ";
    cin >> m >> n >> k;
    int max1 = std::max({a, b, c}, __comp);
    int min1 = std::min({a, b, c}, __comp);
    int mid1 = __mid(a, b, c);

    int max2 = std::max({m, n, k}, __comp);
    int min2 = std::min({m, n, k}, __comp);
    int mid2 = __mid(m, n, k);

    if ((max1 >= max2 and mid1 >= mid2 and min1 >= min2) 
        or (max1 <= max2 and mid1 <= mid2 and min1 <= min2))
            cout << "Коробки помещаются одна в другую полностью\n\n";
    else cout << "Полностью коробки друг в друга не помещаются\n\n";

    system("pause");
    return 0;
}
