#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int speed, height;
    cout << "Введите высоту полёта: ";
    cin >> height;
    cout << "Введите текущую скорость: ";
    cin >> speed;

    if ((speed >= 750 and speed <= 850) and (height >= 9000 and height <= 9500))
        cout << "Полёт проходит штатно\n\n";
    else cout << "Скорректируйте показатели полёта!!!\n\n";
    
    
    system("pause");
    return 0;
}
