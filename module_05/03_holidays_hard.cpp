#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int date;
    cout << "Введите число мая: ";
    cin >> date;

    int dayStart;
    cout << "Введите номер дня недели (от 1 до 7), с которого начинается месяц: ";
    cin >> dayStart;

    if (dayStart < 1 or dayStart > 7) {
        cout << "Некорректный день недели\n\n";
        system("pause");
        return 0;
    }

    int sat = 7 - dayStart;
    int sun = (sat + 1) % 7;

    if (date < 1 or date > 31) cout << "Введено некорректное число\n\n";
    else if ((date >= 1 and date <= 5) or (date >= 8 and date <= 10 ) or (date % 7 == sat or date % 7 == sun))
        cout << "Выходной день\n\n";
    else cout << "Рабочий день\n\n";
    
    
    system("pause");
    return 0;
}
