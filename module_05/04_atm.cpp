#include <iostream>
#include <windows.h>

using namespace std;

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int cashRequest, count100, count200, count500, count1000, count2000, count5000;

    cout << "Добро пожаловать!\nВведите требуемую сумму: ";
    cin >> cashRequest;
    if (cashRequest < 0 or cashRequest > 150000 or (cashRequest % 100)) 
        cout << "Данную сумму выдать невозможно\n";
    else {
        count5000 = cashRequest / 5000;
        cashRequest -= count5000 * 5000;
        count2000 = cashRequest / 2000;
        cashRequest -= count2000 * 2000;
        count1000 = cashRequest / 1000;
        cashRequest -= count1000 * 1000;
        count500 = cashRequest / 500;
        cashRequest -= count500 * 500;
        count200 = cashRequest / 200;
        cashRequest -= count200 * 200;
        count100 = cashRequest / 100;
        cashRequest -= count100 * 100;
    
        cout << "\nВозьмите ваши банкноты:\n";
        if (count5000) cout << "5000 * " << count5000 << "\n";
        if (count2000) cout << "2000 * " << count2000 << "\n";
        if (count1000) cout << "1000 * " << count1000 << "\n";
        if (count500) cout << "500 * " << count500 << "\n";
        if (count200) cout << "200 * " << count200 << "\n";
        if (count100) cout << "100 * " << count100 << "\n";
    }
    
    cout << "\n";
    system("pause");
    return 0;
}
