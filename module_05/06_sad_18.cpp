#include <iostream>
#include <windows.h>

using namespace std;

int dayInMonth (int month, int year){
    if (month == 4 or month == 6 or month == 9 or month == 11)
        return 30;
    else if (month == 2){
        if(!(year % 4)){
            if (year % 400 == 0 || year % 100 != 0)
                return 29;
        }
    return 28;
    }
    else return 31;
}

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    int bDay, bMonth, bYear;
    int curDay, curMonth, curYear;
    bool accepted = false;
    cout << "Введите дату рождения (в формате дд мм гггг): ";
    cin >> bDay >> bMonth >> bYear;
    if (bDay < 0 or bDay > dayInMonth(bMonth, bYear) or bMonth < 1 or bMonth > 12){
        cout << "Введена неправильная дата\n\n";
        system("pause");
        return 0;
    }
    cout << "Введите текущую дату (в формате дд мм гггг): ";
    cin >> curDay >> curMonth >> curYear;
    if (curDay < 0 or curDay > dayInMonth(curMonth, curYear) or curMonth < 1 or curMonth > 12){
        cout << "Введена неправильная дата\n\n";
        system("pause");
        return 0;
    }  
    
    if (curYear - bYear > 18)
        accepted = true;
    else if (curYear - bYear == 18){
        if (curMonth > bMonth) accepted = true;
        else if (curMonth == bMonth)
            if (curDay > bDay) accepted = true;
    }

    cout << "Продажа алкоголя ";
    if (accepted) cout << "разрешена\n\n";
    else cout << "ЗАПРЕЩЕНА!\n\n";
    
    
    
    system("pause");
    return 0;
}
